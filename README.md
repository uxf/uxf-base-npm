# UXF NPM Packages

## List of packages

| Package                                                 | Description                      |
| ------------------------------------------------------- | -------------------------------- |
| [@uxf/translations](packages/uxf-translations/README.md)| Universal translations           |

## How to develop

```bash
$ git clone git@gitlab.com:uxf/uxf-base-npm.git
$ cd uxf-base-npm
$ npm install

// link node modules to packages
$ lerna bootstrap

// start dev server on http://localhost:3000 -- runs all packages
$ npm run dev

// publish all npm packages which have changed version
# npm package will be deployed after merge to master
```

## Install new dependecy to package
```bash
lerna add packageName --scope=@uxf/data-grid  
```

```bash
lerna add packageName --scope=@uxf/data-grid  --dev // dev dependency
```

```bash
lerna add packageName --scope=@uxf/data-grid  --peer // peer dependency
```


## Sources

[Lerna Documentation](https://github.com/lerna/lerna/tree/master/commands/publish)
