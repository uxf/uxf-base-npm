module.exports = {
    roots: ["<rootDir>/src"],
    transform: {
        "\\.(ts|tsx)$": "ts-jest",
    },
    verbose: true,
    testRegex: ".*\\.test.(ts|tsx|js)$",
    moduleFileExtensions: ["ts", "tsx", "js", "jsx"],
    moduleDirectories: ["node_modules", "src"],
};
