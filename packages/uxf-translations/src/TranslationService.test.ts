import { TranslationService } from "./index";
import { PluralResolverMap } from "./types";

const translations = {
    plurals: {
        cs: {
            few: "{{count}} nové zprávy",
            many: "{{count}} nové zprávy",
            one: "1 nová zpráva",
            other: "{{count}} nových zpráv",
        },
        en: {
            one: "1 new message",
            other: "{{count}} new messages",
        },
    },
    post: {
        cs: {
            few: "Příspěvky",
            many: "Příspěvků",
            one: "Příspěvek",
            other: "Příspěvků",
        },
        en: {
            one: "Post",
            other: "Posts",
        },
    },
    simpleKey: {
        cs: "Jednoduchý klíč",
        en: "Simple key",
    },
};

enum Locales {
    en = "en",
    cs = "cs",
}
type TranslationKeys = keyof typeof translations;

const pluralResolverMap: PluralResolverMap = {
    [Locales.cs]: count =>
        count % 1 !== 0 ? "many" : count === 1 ? "one" : count >= 2 && count <= 4 ? "few" : "other",
    [Locales.en]: count => (count === 1 ? "one" : "other"),
};

const translationService = new TranslationService<TranslationKeys, Locales>(
    translations,
    Locales.cs,
    pluralResolverMap,
);

describe("Translation service", () => {
    it("plural with count = 0", () => {
        expect(translationService.translate("post", { count: 0 })).toBe("Příspěvků");
    });

    it("t()", () => {
        expect(translationService.translate("plurals", { count: 0.25 })).toBe("0.25 nové zprávy");
        expect(translationService.translate("plurals", { count: 1 })).toBe("1 nová zpráva");
        expect(translationService.translate("plurals", { count: 2 })).toBe("2 nové zprávy");
        expect(translationService.translate("plurals", { count: 3 })).toBe("3 nové zprávy");
        expect(translationService.translate("plurals", { count: 5 })).toBe("5 nových zpráv");

        expect(translationService.translate("simpleKey")).toBe("Jednoduchý klíč");

        translationService.changeLocale(Locales.en);

        expect(translationService.translate("plurals", { count: 1 })).toBe("1 new message");
        expect(translationService.translate("plurals", { count: 2 })).toBe("2 new messages");

        expect(translationService.translate("simpleKey")).toBe("Simple key");
    });

    it("t() - missing key", () => {
        expect(translationService.translate("missing-key" as any)).toBe("missing-key");
    });
});
