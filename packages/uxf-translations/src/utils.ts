export const interpolate = (text: string, params: any): string => {
    let newText = text;
    for (const key in params) {
        if (params.hasOwnProperty(key)) {
            newText = newText.replace(`{{${key}}}`, params[key]);
        }
    }
    return newText;
};