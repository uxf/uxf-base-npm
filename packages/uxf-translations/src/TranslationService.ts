import { Plural, PluralResolverMap, TranslationSource } from "./types";
import { interpolate } from "./utils";

export class TranslationService<TranslationKeys extends string = string, AllowedLocales extends string = any> {
    private locale: AllowedLocales;
    private translations: TranslationSource;
    private pluralResolverMap: PluralResolverMap;

    constructor(translations: TranslationSource, locale: AllowedLocales, pluralizations: PluralResolverMap = {}) {
        this.locale = locale;
        this.translations = translations;
        this.pluralResolverMap = pluralizations;
    }

    public changeTranslations = (translations: TranslationSource) => {
        this.translations = translations;
    };

    public changeLocale = (locale: AllowedLocales): void => {
        this.locale = locale;
    };

    public translate = (key: TranslationKeys, params?: any): string => {
        let phrase = this.translations[key] && this.translations[key][this.locale];

        if (!phrase) {
            return key;
        }

        if (typeof phrase === "object" && phrase !== null && params && typeof params.count !== "undefined") {
            const newPhrase = this.pluralize(phrase, params.count);
            if (newPhrase) {
                phrase = newPhrase;
            }
        }

        if (typeof phrase === "string") {
            return params ? interpolate(phrase, params) : phrase;
        }

        return key;
    };

    private pluralize = (plural: Plural, count: number): string | undefined => {
        const pluralKey = this.pluralResolverMap[this.locale](count);

        return plural && plural[pluralKey];
    };
}
