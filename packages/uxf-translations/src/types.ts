export interface Plural {
    readonly zero?: string;
    readonly one?: string;
    readonly few?: string;
    readonly many?: string;
    readonly other?: string;
}

export interface TranslationItem {
    [locale: string]: string | Plural
};

export interface TranslationSource {
    [key: string]: TranslationItem
}

export type PluralResolver = (count: number) => keyof Plural

export interface PluralResolverMap {
    [locale: string]: PluralResolver
}