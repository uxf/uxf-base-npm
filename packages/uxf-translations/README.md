# @uxf/translations

## Installation
```
yarn add @uxf/translations

// or

npm install @uxf/translations --save
```

## How to use

```typescript
// translations.ts
export const translations = {
    key1: {
        cs: "Klíč 1",
        en: "Key 1",
    },
    plural: {
        cs: {
            one: "1 nová zpráva",
            few: "{{count}} nové zprávy",
            many: "{{count}} nových zpráv",
            other: "{{count}} nových zpráv",
        },
        en: {
            one: "1 new message",
            other: "{{count}} new messages",
        },
    },
};
```


```typescript
// config.ts
import { TranslationService } from "@uxf/translations";
import { translations } from "./translations";

type TranslationKeys = keyof typeof translations;

enum Locales {
    en = "en",
    cs = "cs",
}

const pluralResolverMap: PluralResolverMap = {
    [Locales.cs]: count =>
        count % 1 !== 0 ? "many" : count === 1 ? "one" : count >= 2 && count <= 4 ? "few" : "other",
    [Locales.en]: count => (count === 1 ? "one" : "other"),
};

export const translationService = new TranslationService<TranslationKeys, Locales>(
    translations,
    Locales.cs,
    pluralResolverMap,
);
```

```typescript jsx
import { translationService } from "./config";

export const Title: React.FC = () => {
    return <h1>{translationService.translate("plural", { count: 3 })}</h1>;
}
```