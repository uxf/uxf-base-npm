import React, { FC, Fragment } from "react";
import { renderElement } from "./renderElement";
import { renderLeaf } from "./renderLeaf";
import { isLeaf } from "./typeGuards";
import { ContentRendererProps } from "./types";

export const ContentRenderer: FC<ContentRendererProps> = ({ data, components, defaultComponentProps }) => {
    if (!data) {
        return null;
    }

    return (
        <>
            {data.map((item, index) => {
                if (isLeaf(item)) {
                    return (
                        <React.Fragment key={index}>
                            {renderLeaf(item, components, defaultComponentProps)}
                        </React.Fragment>
                    );
                }

                return <Fragment key={index}>{renderElement(item, components, defaultComponentProps)}</Fragment>;
            })}
        </>
    );
};
