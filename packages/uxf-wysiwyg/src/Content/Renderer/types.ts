import { EditorImageElement, EditorLinkElement, EditorVideoElement, WysiwygContent } from "../../types";

export interface RenderComponentProps {
    children: import("react").ReactNode;
}

export interface RenderImageComponentProps {
    image: EditorImageElement;
}

export interface RenderLinkComponentProps extends RenderComponentProps {
    link: EditorLinkElement;
}

export interface RenderVideoComponentProps extends Omit<RenderComponentProps, "children"> {
    video: EditorVideoElement;
}

export type VideoWrapperProps = import("react").DetailedHTMLProps<
    import("react").HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
>;

export type RenderComponent = import("react").ComponentType<RenderComponentProps>;
export type RenderImageComponent = import("react").ComponentType<RenderImageComponentProps>;
export type RenderLinkComponent = import("react").ComponentType<RenderLinkComponentProps>;
export type RenderVideoComponent = import("react").ComponentType<RenderVideoComponentProps>;
export type VideoWrapper = import("react").ComponentType<any>;

export interface RenderComponents {
    paragraphComponent?: RenderComponent;
    h1Component?: RenderComponent;
    h2Component?: RenderComponent;
    h3Component?: RenderComponent;
    h4Component?: RenderComponent;
    h5Component?: RenderComponent;
    h6Component?: RenderComponent;
    bulletedListComponent?: RenderComponent;
    numberedListComponent?: RenderComponent;
    listItemComponent?: RenderComponent;
    blockQuoteComponent?: RenderComponent;
    boldComponent?: RenderComponent;
    codeComponent?: RenderComponent;
    underlineComponent?: RenderComponent;
    italicComponent?: RenderComponent;
    imageComponent?: RenderImageComponent;
    linkComponent?: RenderLinkComponent;
    videoWrapper?: VideoWrapper;
}

export interface DefaultComponentProps {
    a?: Omit<
        import("react").DetailedHTMLProps<import("react").AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement>,
        "href"
    >;
    blockquote?: import("react").DetailedHTMLProps<import("react").BlockquoteHTMLAttributes<HTMLElement>, HTMLElement>;
    code?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLElement>, HTMLElement>;
    em?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLElement>, HTMLElement>;
    h1?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;
    h2?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;
    h3?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;
    h4?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;
    h5?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;
    h6?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>;
    li?: import("react").DetailedHTMLProps<import("react").LiHTMLAttributes<HTMLLIElement>, HTMLLIElement>;
    ol?: import("react").DetailedHTMLProps<import("react").OlHTMLAttributes<HTMLOListElement>, HTMLOListElement>;
    p?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLParagraphElement>, HTMLParagraphElement>;
    strong?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLElement>, HTMLElement>;
    u?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLElement>, HTMLElement>;
    ul?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLUListElement>, HTMLUListElement>;
    image?: {
        divWrapper?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLDivElement>, HTMLDivElement>;
        img?: Omit<
            import("react").DetailedHTMLProps<import("react").ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement>,
            "src"
        >;
        figure?: Omit<
            import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLElement>, HTMLElement>,
            "contentEditable"
        >;
        figcaption?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLElement>, HTMLElement>;
        cite?: import("react").DetailedHTMLProps<import("react").HTMLAttributes<HTMLElement>, HTMLElement>;
    };
    video?: {
        divWrapper?: VideoWrapperProps;
    };
}

export interface ContentRendererProps {
    data: WysiwygContent;
    components?: RenderComponents;
    defaultComponentProps?: DefaultComponentProps;
}
