import React, { ReactNode } from "react";
import { EditorLeafElement } from "../../types";
import { DefaultComponentProps, RenderComponents } from "./types";

export const renderLeaf = (
    leaf: EditorLeafElement,
    components?: RenderComponents,
    defaultComponentProps?: DefaultComponentProps,
): ReactNode => {
    let content: React.ReactNode = <>{leaf.text}</>;

    if (leaf.text === "") {
        return <>&#65279;</>;
    }

    if (leaf.bold) {
        if (components?.boldComponent) {
            return <components.boldComponent>{content}</components.boldComponent>;
        }

        return <strong {...defaultComponentProps?.strong}>{content}</strong>;
    }

    if (leaf.code) {
        if (components?.codeComponent) {
            return <components.codeComponent>{content}</components.codeComponent>;
        }

        return <code {...defaultComponentProps?.code}>{content}</code>;
    }

    if (leaf.italic) {
        if (components?.italicComponent) {
            return <components.italicComponent>{content}</components.italicComponent>;
        }

        return <em {...defaultComponentProps?.em}>{content}</em>;
    }

    if (leaf.underline) {
        if (components?.underlineComponent) {
            return <components.underlineComponent>{content}</components.underlineComponent>;
        }

        return <u {...defaultComponentProps?.u}>{content}</u>;
    }

    return content;
};
