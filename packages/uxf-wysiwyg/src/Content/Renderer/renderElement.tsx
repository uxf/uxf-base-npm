import React, { Fragment, ReactNode } from "react";
import {
    EditorImageElement,
    EditorLinkElement,
    EditorVideoElement,
    SupportedBlockElements,
    SupportedBlockElementTypes,
} from "../../types";
import { VideoComponent } from "./Components/VideoComponent/VideoComponent";
import { renderLeaf } from "./renderLeaf";
import { hasUrl, isLeaf } from "./typeGuards";
import { DefaultComponentProps, RenderComponents } from "./types";

export const renderElement = (
    element: SupportedBlockElements,
    components?: RenderComponents,
    defaultComponentProps?: DefaultComponentProps,
): ReactNode => {
    const children = element.children.map((item, index) => {
        if (isLeaf(item)) {
            return <Fragment key={index}>{renderLeaf(item, components, defaultComponentProps)}</Fragment>;
        } else {
            return <Fragment key={index}>{renderElement(item, components, defaultComponentProps)}</Fragment>;
        }
    });

    switch (element.type as SupportedBlockElementTypes) {
        // Quote
        case "paragraph":
            if (components?.paragraphComponent) {
                return <components.paragraphComponent>{children}</components.paragraphComponent>;
            }

            return <p {...defaultComponentProps?.p}>{children}</p>;
        // Headlines
        case "h1":
            if (components?.h1Component) {
                return <components.h1Component>{children}</components.h1Component>;
            }

            return <h1 {...defaultComponentProps?.h1}>{children}</h1>;
        case "h2":
            if (components?.h2Component) {
                return <components.h2Component>{children}</components.h2Component>;
            }

            return <h2 {...defaultComponentProps?.h2}>{children}</h2>;
        case "h3":
            if (components?.h3Component) {
                return <components.h3Component>{children}</components.h3Component>;
            }

            return <h3 {...defaultComponentProps?.h3}>{children}</h3>;
        case "h4":
            if (components?.h4Component) {
                return <components.h4Component>{children}</components.h4Component>;
            }

            return <h4 {...defaultComponentProps?.h4}>{children}</h4>;
        case "h5":
            if (components?.h5Component) {
                return <components.h5Component>{children}</components.h5Component>;
            }

            return <h5 {...defaultComponentProps?.h4}>{children}</h5>;
        case "h6":
            if (components?.h6Component) {
                return <components.h6Component>{children}</components.h6Component>;
            }

            return <h6 {...defaultComponentProps?.h4}>{children}</h6>;
        // Lists
        case "bulleted-list":
            if (components?.bulletedListComponent) {
                return <components.bulletedListComponent>{children}</components.bulletedListComponent>;
            }

            return <ul {...defaultComponentProps?.ul}>{children}</ul>;
        case "list-item":
            if (components?.listItemComponent) {
                return <components.listItemComponent>{children}</components.listItemComponent>;
            }

            return <li {...defaultComponentProps?.li}>{children}</li>;
        case "numbered-list":
            if (components?.numberedListComponent) {
                return <components.numberedListComponent>{children}</components.numberedListComponent>;
            }

            return <ol {...defaultComponentProps?.ol}>{children}</ol>;
        // Quote
        case "block-quote":
            if (components?.blockQuoteComponent) {
                return <components.blockQuoteComponent>{children}</components.blockQuoteComponent>;
            }

            return <blockquote {...defaultComponentProps?.blockquote}>{children}</blockquote>;
        case "link":
            if (hasUrl(element)) {
                if (components?.linkComponent) {
                    return (
                        <components.linkComponent link={element as EditorLinkElement}>
                            {children}
                        </components.linkComponent>
                    );
                }

                return (
                    <a {...defaultComponentProps?.a} href={element.url} target={element.target}>
                        {children}
                    </a>
                );
            }

            return <span>{children}</span>;
        case "image":
            if (components?.imageComponent) {
                return <components.imageComponent image={element as EditorImageElement} />;
            }

            return (
                <div
                    style={defaultComponentProps?.image?.divWrapper ? undefined : { marginBottom: "2em" }}
                    {...defaultComponentProps?.image?.divWrapper}
                >
                    <figure {...defaultComponentProps?.image?.figure} contentEditable={false}>
                        <img {...defaultComponentProps?.image?.img} src={element.url} alt={element.alt} />
                        {(element.caption || element.source) && (
                            <figcaption {...defaultComponentProps?.image?.figcaption}>
                                {element.caption}
                                {element.source && (
                                    <>
                                        {element.caption && <br />}
                                        <cite {...defaultComponentProps?.image?.cite}>{element.source}</cite>
                                    </>
                                )}
                            </figcaption>
                        )}
                    </figure>
                    {children}
                </div>
            );
        case "video":
            return (
                <VideoComponent
                    video={element as EditorVideoElement}
                    wrapperProps={defaultComponentProps?.video?.divWrapper}
                    VideoWrapper={components?.videoWrapper}
                />
            );
        default:
            if (components?.paragraphComponent) {
                return <components.paragraphComponent>{children}</components.paragraphComponent>;
            }

            return <p {...defaultComponentProps?.p}>{children}</p>;
    }
};
