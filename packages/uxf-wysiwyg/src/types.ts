import { RenderLeafProps as SlateRenderLeafProps } from "slate-react";

export interface WysiwygEditorProps {
    value?: any;
    onChange: (event: any) => void;
    imageUrlClient: ImageUrlClient;
    imageNamespace: string;
    onUploadImage: ImageUploadHandler;
    disabledActions?: DisabledActions;
}

export type ImageUrlClient = (uuid: string, extension: string, namespace?: string) => string; // storageClient TODO dependency
export type ImageUploadHandler = (
    file: File,
    namespace: string,
) => Promise<{
    id: number;
    uuid: string;
    type: string;
    extension: string;
    name: string;
    namespace?: string | null;
    url?: string;
}>;

export type FontStyle = "italic" | "bold" | "underline" | "code";
export type LeafMarks = FontStyle;

export type Headings = "h1" | "h2" | "h3" | "h4" | "h5" | "h6" | "paragraph";
export type Lists = "bulleted-list" | "list-item" | "numbered-list";
export type SupportedBlockElementTypes = Headings | Lists | "block-quote" | "link" | "image" | "video";

export type EditorBlockElementChildren = Array<SupportedBlockElements & EditorLeafElement>;

export interface EditorBlockElement {
    type: SupportedBlockElementTypes;
    children: EditorBlockElementChildren;
    [key: string]: any;
}

type PickedLinkProps = Pick<
    import("react").DetailedHTMLProps<import("react").AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement>,
    "target" | "rel" | "download" | "hrefLang" | "media" | "referrerPolicy"
>;

interface OwnLinkProps extends EditorBlockElement {
    type: "link";
    url: string;
}

export type EditorLinkElement = PickedLinkProps & OwnLinkProps;

export interface EditorImageElement extends EditorBlockElement {
    type: "image";
    uuid?: string;
    extension?: string;
    name?: string;
    namespace?: string;
    source?: string;
    caption?: string;
    alt?: string;
    url?: string;
}

export interface EditorVideoElement extends EditorBlockElement {
    type: "video";
    url: string;
}

export type SupportedBlockElements = EditorBlockElement | EditorImageElement | EditorLinkElement | EditorVideoElement;

/**
 * Marks are either present and true or undefined
 */
export interface EditorLeafElement {
    text: string;
    bold?: true;
    italic?: true;
    underline?: true;
    code?: true;
    [key: string]: unknown;
}

export interface RenderLeafProps extends SlateRenderLeafProps {
    leaf: EditorLeafElement;
}

export interface DisabledActions {
    h1?: boolean;
    h2?: boolean;
    h3?: boolean;
    h4?: boolean;
    h5?: boolean;
    h6?: boolean;
    bulletedList?: boolean;
    numberedList?: boolean;
    blockQuote?: boolean;
    bold?: boolean;
    code?: boolean;
    underline?: boolean;
    italic?: boolean;
    image?: boolean;
    link?: boolean;
    video?: boolean;
}

export type WysiwygContent = Array<SupportedBlockElements | EditorLeafElement>;
