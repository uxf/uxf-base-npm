import React from "react";
import ReactPlayer from "react-player/lazy";
import { RenderElementProps, useFocused, useSelected } from "slate-react";
import { useVideoStyles } from "./styles";

export const VideoElement = ({ attributes, element }: Omit<RenderElementProps, "children">) => {
    const selected = useSelected();
    const focused = useFocused();
    const classes = useVideoStyles({ selected, focused });
    return (
        <div {...attributes} className={classes.wrapper} id="aaaa">
            <ReactPlayer
                className={classes.player}
                url={(element.url as string) ?? ""}
                controls={true}
                width="100%"
                height="100%"
            />
        </div>
    );
};
