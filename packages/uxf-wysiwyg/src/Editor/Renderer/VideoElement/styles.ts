import { Theme } from "@material-ui/core/styles/createMuiTheme";
import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

interface VideoStyleProps {
    focused: boolean;
    selected: boolean;
}

export const useVideoStyles = makeStyles<Theme, VideoStyleProps, "player" | "wrapper">(theme =>
    createStyles({
        player: {
            position: "absolute",
            top: 0,
            left: 0,
            maxWidth: "100%",
            maxHeight: "100%",
            boxShadow: ({ selected, focused }) =>
                selected && focused ? `0 0 0 3px ${theme.palette.action.active}` : "none",
        },
        wrapper: {
            position: "relative",
            paddingTop: "56.25%" /* 720 / 1280 = 0.5625 */,
        },
    }),
);
