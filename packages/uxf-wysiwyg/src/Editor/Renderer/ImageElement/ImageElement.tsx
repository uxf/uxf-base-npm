import React from "react";
import { RenderElementProps, useFocused, useSelected } from "slate-react";
import { ImageUrlClient } from "../../../types";
import { useImageStyles } from "./styles";

export const ImageElement = ({
    attributes,
    element,
    imageUrlClient,
    imageNamespace,
    children,
}: RenderElementProps & { imageUrlClient: ImageUrlClient; imageNamespace: string }) => {
    const selected = useSelected();
    const focused = useFocused();
    const classes = useImageStyles({ selected, focused });

    return (
        <div {...attributes} style={{ marginBottom: "2em", width: "100%" }}>
            <figure contentEditable={false}>
                <img
                    src={imageUrlClient(element.uuid as string, element.extension as string, imageNamespace)}
                    className={classes.image}
                    alt={element.alt as string}
                />
                {(element.caption || element.source) && (
                    <figcaption>
                        {element.caption}
                        {element.source && (
                            <>
                                {element.caption && <br />}
                                <cite>{element.source as string}</cite>
                            </>
                        )}
                    </figcaption>
                )}
            </figure>
            {children}
        </div>
    );
};
