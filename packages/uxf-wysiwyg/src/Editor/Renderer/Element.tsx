import Typography from "@material-ui/core/Typography/Typography";
import React, { ReactElement } from "react";
import { RenderElementProps } from "slate-react";
import { ImageUrlClient, SupportedBlockElementTypes } from "../../types";
import { ImageElement } from "./ImageElement";
import { VideoElement } from "./VideoElement";

export const Element = (
    props: RenderElementProps & { imageUrlClient: ImageUrlClient; imageNamespace: string },
): ReactElement => {
    const { attributes, children, element, imageNamespace, imageUrlClient } = props;

    switch (element.type as SupportedBlockElementTypes) {
        case "paragraph":
            return (
                <Typography variant="body1" {...attributes} style={{ marginBottom: "1em" }}>
                    {children}
                </Typography>
            );
        // Headlines
        case "h1":
            return <h1 {...attributes}>{children}</h1>;
        case "h2":
            return <h2 {...attributes}>{children}</h2>;
        case "h3":
            return <h3 {...attributes}>{children}</h3>;
        case "h4":
            return <h4 {...attributes}>{children}</h4>;
        case "h5":
            return <h5 {...attributes}>{children}</h5>;
        case "h6":
            return <h6 {...attributes}>{children}</h6>;
        // Lists
        case "bulleted-list":
            return <ul {...attributes}>{children}</ul>;
        case "list-item":
            return <li {...attributes}>{children}</li>;
        case "numbered-list":
            return <ol {...attributes}>{children}</ol>;
        // Quote
        case "block-quote":
            return <blockquote {...attributes}>{children}</blockquote>;
        case "link":
            return (
                <a
                    {...attributes}
                    href={(element.url as string) ?? ""}
                    target={(element.target as string) ?? undefined}
                >
                    {children}
                </a>
            );
        case "image":
            return <ImageElement {...props} imageUrlClient={imageUrlClient} imageNamespace={imageNamespace} />;
        case "video":
            return <VideoElement {...props} />;
        default:
            return <span {...attributes}>{children}</span>;
    }
};
