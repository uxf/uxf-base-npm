import isUrl from "is-url";
import { Editor, Range, Transforms } from "slate";
import { jsx } from "slate-hyperscript";
import {
    EditorImageElement,
    EditorLinkElement,
    EditorVideoElement,
    LeafMarks,
    SupportedBlockElementTypes,
} from "../../types";

const LIST_TYPES = ["numbered-list", "bulleted-list"];

export const isMarkActive = (editor: Editor, mark: LeafMarks): boolean => {
    const marks = Editor.marks(editor);
    return marks ? marks[mark] === true : false;
};

export const toggleMark = (editor: Editor, format: LeafMarks): void => {
    const isActive = isMarkActive(editor, format);

    if (isActive) {
        Editor.removeMark(editor, format);
    } else {
        Editor.addMark(editor, format, true);
    }
};

export const isElementActive = (editor: Editor, format: SupportedBlockElementTypes): boolean => {
    const [match] = Editor.nodes(editor, {
        match: n => n.type === format,
    });

    return !!match;
};

export const toggleElement = (editor: Editor, format: SupportedBlockElementTypes): void => {
    const isActive = isElementActive(editor, format);
    const isList = LIST_TYPES.includes(format);

    Transforms.unwrapNodes(editor, {
        match: n => LIST_TYPES.includes(n.type as string),
        split: true,
    });

    Transforms.setNodes(editor, {
        type: isActive ? "paragraph" : isList ? "list-item" : format,
    });

    if (!isActive && isList) {
        const block = { type: format, children: [] };
        Transforms.wrapNodes(editor, block);
    }
};

export const isLinkActive = (editor: Editor): boolean => {
    const [link] = Editor.nodes(editor, { match: n => n.type === "link" });
    return !!link;
};

export const getLinkUrl = (editor: Editor): string | null => {
    if (isLinkActive(editor)) {
        const [link] = Editor.nodes(editor, { match: n => n.type === "link" });
        return link[0]?.url as string;
    }

    return null;
};

export const getActiveLink = (editor: Editor): EditorLinkElement | null => {
    if (isLinkActive(editor)) {
        const [link] = Editor.nodes(editor, { match: n => n.type === "link" });
        return link[0] as EditorLinkElement;
    }

    return null;
};

const unwrapLink = (editor: Editor): void => {
    Transforms.unwrapNodes(editor, { match: n => n.type === "link" });
};

const wrapLink = (editor: Editor, url: string, text?: string, target?: "_blank"): void => {
    if (isLinkActive(editor)) {
        unwrapLink(editor);
    }

    const { selection } = editor;
    const isCollapsed = selection && Range.isCollapsed(selection);
    const link = {
        type: "link",
        url,
        target,
        children: isCollapsed ? [{ text: text ? text : url }] : [],
    };

    if (isCollapsed) {
        Transforms.insertNodes(editor, link);
    } else {
        Transforms.wrapNodes(editor, link, { split: true });
        Transforms.collapse(editor, { edge: "end" });
    }
};

export const insertLink = (editor: Editor, url: string, text?: string, target?: "_blank"): void => {
    if (editor.selection) {
        wrapLink(editor, url, text, target);
    }
};

export const removeLink = (editor: Editor): void => {
    if (editor.selection) {
        unwrapLink(editor);
    }
};

export const insertImage = (editor: Editor, image: EditorImageElement, range?: Range | null): void => {
    Transforms.insertNodes(editor, [jsx("element", { ...image }, [jsx("text")]), createEmptyParagraph()], {
        at: range ? range : undefined,
    });
};

export const isImageActive = (editor: Editor): boolean => {
    const [image] = Editor.nodes(editor, { match: n => n.type === "image" });
    return !!image;
};

export const getActiveImage = (editor: Editor): EditorImageElement | null => {
    if (isImageActive(editor)) {
        const [image] = Editor.nodes(editor, { match: n => n.type === "image" }) as any;
        return image[0] as EditorImageElement;
    }

    return null;
};

export const isVideoActive = (editor: Editor): boolean => {
    const [video] = Editor.nodes(editor, { match: n => n.type === "video" });
    return !!video;
};

export const getActiveVideo = (editor: Editor): EditorVideoElement | null => {
    if (isImageActive(editor)) {
        const [video] = Editor.nodes(editor, { match: n => n.type === "video" }) as any;
        return video[0] as EditorVideoElement;
    }

    return null;
};

export const insertVideo = (editor: Editor, url: string, range?: Range | null): void => {
    Transforms.insertNodes(editor, [jsx("element", { type: "video", url }, [jsx("text")]), createEmptyParagraph()], {
        at: range ? range : undefined,
    });
};

export const createEmptyParagraph = () => jsx("element", { type: "paragraph" }, [jsx("text")]);

export const isValidUrl = (url?: string) => url && isUrl(url);
