import React, { FC, useCallback, useMemo } from "react";
import { createEditor, Element as SlateElement, Node, Transforms } from "slate";
import { withHistory } from "slate-history";
import { Editable, ReactEditor, RenderElementProps, Slate, withReact } from "slate-react";
import { RenderLeafProps, WysiwygEditorProps } from "../types";
import { Element, insertLink, isValidUrl, Leaf } from "./Renderer";
import { Toolbar } from "./Toolbar";

const initialValue: Node[] = [
    {
        type: "paragraph",
        children: [{ text: "" }],
    },
];

// const isImageUrl = (url: string): boolean => {
//     if (!isValidUrl(url)) {
//         return false;
//     }
//     const ext = new URL(url).pathname.split(".").pop();
//     const extensions = ["jpg", "jpeg"];
//     return extensions.includes(ext ?? "");
// };

const withLinks = (editor: ReactEditor): ReactEditor => {
    const { isInline, insertData, normalizeNode } = editor;

    editor.isInline = (element: SlateElement): boolean => {
        return element.type === "link" ? true : isInline(element);
    };

    editor.normalizeNode = entry => {
        const [node, path] = entry;
        // If the element is a paragraph, ensure its children are valid.
        if (SlateElement.isElement(node) && node.type === "paragraph") {
            for (const [child, childPath] of Node.children(editor, path)) {
                if (SlateElement.isElement(child) && !editor.isInline(child)) {
                    Transforms.unwrapNodes(editor, { at: childPath });
                    return;
                }
            }
        }

        // Fall back to the original `normalizeNode` to enforce other constraints. http://uxf.cz
        normalizeNode(entry);
    };

    editor.insertData = (data: DataTransfer) => {
        const text = data.getData("text/plain");
        if (isValidUrl(text)) {
            insertLink(editor, text, text);
        } else {
            insertData(data);
        }
    };

    return editor;
};

const withImages = (editor: ReactEditor): ReactEditor => {
    const { isVoid } = editor;

    editor.isVoid = (element: SlateElement) => {
        return element.type === "image" || element.type === "video" ? true : isVoid(element);
    };

    // editor.insertData = (data: DataTransfer) => {
    //     const text = data.getData("text/plain");
    //     const { files } = data;
    //
    //     if (files && files.length > 0) {
    //         for (const file of files) {
    //             const reader = new FileReader();
    //             const [mime] = file.type.split("/");
    //
    //             if (mime === "image") {
    //                 reader.addEventListener("load", () => {
    //                     const url = reader.result as string;
    //                     insertImage(editor, { type: "image", url, children: [] });
    //                 });
    //
    //                 reader.readAsDataURL(file);
    //             }
    //         }
    //     } else if (isImageUrl(text)) {
    //         insertImage(editor, { type: "image", url: text, children: [] });
    //     } else {
    //         insertData(data);
    //     }
    // };

    return editor;
};

export const WysiwygEditor: FC<WysiwygEditorProps> = ({
    value,
    onChange,
    imageUrlClient,
    imageNamespace,
    onUploadImage,
    disabledActions,
}) => {
    const editor = useMemo(() => withImages(withLinks(withHistory(withReact(createEditor())))), []);
    const renderElement = useCallback(
        (props: RenderElementProps) => (
            <Element {...props} imageUrlClient={imageUrlClient} imageNamespace={imageNamespace} />
        ),
        [],
    );
    const renderLeaf = useCallback((props: RenderLeafProps) => <Leaf {...props} />, []);

    return (
        <Slate editor={editor} value={value || initialValue} onChange={onChange}>
            <Toolbar
                imageUploadHandler={onUploadImage}
                imageNamespace={imageNamespace}
                disabledActions={disabledActions}
            />
            <Editable
                renderElement={renderElement}
                spellCheck={false}
                renderLeaf={renderLeaf}
                placeholder="Napište něco hezkého..."
            />
        </Slate>
    );
};
