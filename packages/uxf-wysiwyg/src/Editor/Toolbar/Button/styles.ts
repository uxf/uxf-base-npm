import { Theme } from "@material-ui/core/styles/createMuiTheme";
import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

export const styles = makeStyles((theme: Theme) =>
    createStyles({
        button: {
            margin: theme.spacing(0.5),
            border: "none !important",
            padding: theme.spacing(0, 1),
            "&:not(:first-child)": {
                borderRadius: theme.shape.borderRadius,
            },
            "&:first-child": {
                borderRadius: theme.shape.borderRadius,
            },
        },
    }),
);
