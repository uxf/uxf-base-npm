import { ToggleButtonProps } from "@material-ui/lab/ToggleButton/ToggleButton";
import { ReactNode } from "react";
import { SupportedBlockElementTypes } from "../../../types";

export interface ButtonProps<T = SupportedBlockElementTypes> extends Partial<Pick<ToggleButtonProps, "size">> {
    name: T;
    label: ReactNode;
}
