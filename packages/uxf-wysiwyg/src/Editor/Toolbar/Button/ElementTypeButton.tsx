import ToggleButton from "@material-ui/lab/ToggleButton";
import React, { FC } from "react";
import { useSlate } from "slate-react";
import { isElementActive, toggleElement } from "../../Renderer";
import { styles } from "./styles";
import { ButtonProps } from "./types";

export const ElementTypeButton: FC<ButtonProps> = ({ name, label, size }) => {
    const classes = styles();
    const editor = useSlate();
    return (
        <ToggleButton
            key={name}
            className={classes.button}
            size={size}
            value={name}
            aria-label={name}
            selected={isElementActive(editor, name)}
            onMouseDown={event => {
                toggleElement(editor, name);
                event.preventDefault();
            }}
        >
            {label}
        </ToggleButton>
    );
};

ElementTypeButton.defaultProps = {
    size: "small",
};
