import Image from "@material-ui/icons/Image";
import ToggleButton from "@material-ui/lab/ToggleButton";
import React, { FC, useCallback, useState } from "react";
import { Range } from "slate";
import { useSlate } from "slate-react";
import { EditorImageElement, ImageUploadHandler } from "../../../types";
import { getActiveImage, isImageActive } from "../../Renderer";
import { ImageUploadDialog } from "../Dialog";
import { styles } from "./styles";
import { ButtonProps } from "./types";

export const ImageElementButton: FC<Partial<ButtonProps> & {
    imageUploadHandler: ImageUploadHandler;
    imageNamespace: string;
}> = ({ name, imageNamespace, size, imageUploadHandler }) => {
    const classes = styles();
    const editor = useSlate();
    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [selection, setSelection] = useState<Range | null>(null);
    const [activeImage, setActiveImage] = useState<EditorImageElement | null>(null);

    const closeDialog = useCallback(() => setDialogOpen(false), []);

    return (
        <>
            <ToggleButton
                key={name}
                className={classes.button}
                size={size}
                value={name}
                aria-label={name}
                selected={isImageActive(editor)}
                onClick={e => {
                    e.preventDefault();
                    e.persist();
                    setSelection(editor.selection);
                    setActiveImage(getActiveImage(editor));
                    setDialogOpen(true);
                }}
            >
                <Image />
            </ToggleButton>
            <ImageUploadDialog
                open={dialogOpen}
                onClose={closeDialog}
                imageUploadHandler={imageUploadHandler}
                imageNamespace={imageNamespace}
                editor={editor}
                activeImage={activeImage}
                selection={selection}
            />
        </>
    );
};

ImageElementButton.defaultProps = {
    size: "small",
    name: "image",
};
