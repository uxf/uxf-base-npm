import ToggleButton from "@material-ui/lab/ToggleButton";
import React, { FC } from "react";
import { useSlate } from "slate-react";
import { LeafMarks } from "../../../types";
import { isMarkActive, toggleMark } from "../../Renderer";
import { styles } from "./styles";
import { ButtonProps } from "./types";

export const LeafTypeButton: FC<ButtonProps<LeafMarks>> = ({ name, label, size }) => {
    const classes = styles();
    const editor = useSlate();
    return (
        <ToggleButton
            key={name}
            className={classes.button}
            size={size}
            value={name}
            aria-label={name}
            selected={isMarkActive(editor, name)}
            onMouseDown={event => {
                toggleMark(editor, name);
                event.preventDefault();
            }}
        >
            {label}
        </ToggleButton>
    );
};

LeafTypeButton.defaultProps = {
    size: "small",
};
