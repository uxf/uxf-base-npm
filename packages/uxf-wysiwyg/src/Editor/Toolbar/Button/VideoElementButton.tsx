import VideoIcon from "@material-ui/icons/OndemandVideo";
import ToggleButton from "@material-ui/lab/ToggleButton";
import React, { FC, useCallback, useState } from "react";
import { Range } from "slate";
import { useSlate } from "slate-react";
import { EditorVideoElement } from "../../../types";
import { getActiveVideo, isVideoActive } from "../../Renderer";
import { InsertVideoDialog } from "../Dialog";
import { styles } from "./styles";
import { ButtonProps } from "./types";

const name = "video";

export const VideoElementButton: FC<Partial<Pick<ButtonProps, "size">>> = ({ size = "small" }) => {
    const classes = styles();
    const editor = useSlate();
    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [selection, setSelection] = useState<Range | null>(null);
    const [activeVideo, setActiveVideo] = useState<EditorVideoElement | null>(null);

    const closeDialog = useCallback(() => setDialogOpen(false), []);

    return (
        <>
            <ToggleButton
                key={name}
                className={classes.button}
                size={size}
                value={name}
                aria-label={name}
                selected={isVideoActive(editor)}
                onClick={e => {
                    e.preventDefault();
                    e.persist();
                    setSelection(editor.selection);
                    setActiveVideo(getActiveVideo(editor));
                    setDialogOpen(true);
                }}
            >
                <VideoIcon />
            </ToggleButton>
            <InsertVideoDialog
                open={dialogOpen}
                onClose={closeDialog}
                editor={editor}
                activeVideo={activeVideo}
                selection={selection}
            />
        </>
    );
};
