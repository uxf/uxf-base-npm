import Link from "@material-ui/icons/Link";
import ToggleButton from "@material-ui/lab/ToggleButton";
import React, { FC, useCallback, useState } from "react";
import { Range } from "slate";
import { useSlate } from "slate-react";
import { EditorLinkElement } from "../../../types";
import { getActiveLink, isLinkActive } from "../../Renderer";
import { InsertLinkDialog } from "../Dialog";
import { styles } from "./styles";
import { ButtonProps } from "./types";

export const LinkElementButton: FC<Partial<ButtonProps>> = ({ name, size }) => {
    const classes = styles();
    const editor = useSlate();
    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [selection, setSelection] = useState<Range | null>(null);
    const [activeLink, setActiveLink] = useState<EditorLinkElement | null>(null);

    const closeDialog = useCallback(() => setDialogOpen(false), []);

    return (
        <>
            <ToggleButton
                key={name}
                className={classes.button}
                size={size}
                value={name}
                aria-label={name}
                selected={isLinkActive(editor)}
                onMouseDown={event => {
                    event.preventDefault();
                    event.persist();
                    setSelection(editor.selection);
                    setActiveLink(getActiveLink(editor));
                    setDialogOpen(true);
                    // const url = window.prompt("Vložte URL:", getLinkUrl(editor) ?? "https://");
                    // if (!url || !isUrl(url)) {
                    //     removeLink(editor);
                    //     return;
                    // }
                    //
                    // insertLink(editor, url);
                }}
            >
                <Link />
            </ToggleButton>
            <InsertLinkDialog
                open={dialogOpen}
                onClose={closeDialog}
                editor={editor}
                activeLink={activeLink}
                selection={selection}
            />
        </>
    );
};

LinkElementButton.defaultProps = {
    size: "small",
    name: "link",
};
