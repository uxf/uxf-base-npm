import Divider from "@material-ui/core/Divider";
import Paper from "@material-ui/core/Paper";
import FormatQuoteIcon from "@material-ui/icons/FormatQuote";
import React, { FC } from "react";
import { DisabledActions, ImageUploadHandler } from "../../types";
import { ElementTypeButton, LinkElementButton, ImageElementButton, VideoElementButton } from "./Button";
import { FontStyleGroup, HeadingButtonGroup } from "./ButtonGroup";
import { ListButtonGroup } from "./ButtonGroup/ListButtonGroup";
import { toolbarStyles } from "./styles";

export const Toolbar: FC<{
    imageUploadHandler: ImageUploadHandler;
    imageNamespace: string;
    disabledActions?: DisabledActions;
}> = ({ imageUploadHandler, imageNamespace, disabledActions }) => {
    const classes = toolbarStyles();
    const showFirstDivider =
        !disabledActions?.h1 ||
        !disabledActions?.h2 ||
        !disabledActions?.h3 ||
        !disabledActions?.h4 ||
        !disabledActions?.h5 ||
        !disabledActions?.h6;

    const showSecondDivider =
        !disabledActions?.bold || !disabledActions?.italic || !disabledActions?.underline || !disabledActions?.code;

    const showThirdDivider = !disabledActions?.bulletedList || !disabledActions?.numberedList;

    return (
        <div className={classes.grouped}>
            <Paper elevation={0} className={classes.paper}>
                <HeadingButtonGroup disabledActions={disabledActions} />
                {showFirstDivider && <Divider orientation="vertical" className={classes.divider} />}
                <FontStyleGroup disabledActions={disabledActions} />
                {showSecondDivider && <Divider orientation="vertical" className={classes.divider} />}
                <ListButtonGroup disabledActions={disabledActions} />
                {showThirdDivider && <Divider orientation="vertical" className={classes.divider} />}
                {!disabledActions?.blockQuote && <ElementTypeButton label={<FormatQuoteIcon />} name="block-quote" />}
                {!disabledActions?.link && <LinkElementButton />}
                {!disabledActions?.image && (
                    <ImageElementButton imageUploadHandler={imageUploadHandler} imageNamespace={imageNamespace} />
                )}
                {!disabledActions?.video && <VideoElementButton />}
            </Paper>
        </div>
    );
};
