import Paper from "@material-ui/core/Paper";
import FormatBoldIcon from "@material-ui/icons/FormatBold";
import FormatItalicIcon from "@material-ui/icons/FormatItalic";
import FormatUnderlinedIcon from "@material-ui/icons/FormatUnderlined";
import React, { createRef, FC, useEffect, useState } from "react";
import { createPortal } from "react-dom";
import { Editor, Range } from "slate";
import { ReactEditor, useSlate } from "slate-react";
import { LeafTypeButton, LinkElementButton } from "../Button";
import { toolbarStyles } from "../styles";

export const HoveringToolbar: FC = () => {
    const ref = createRef<any>();
    const classes = toolbarStyles();
    const editor = useSlate();
    const { selection } = editor;
    const w = typeof window !== "undefined" ? window : null;
    const domSelection = w?.getSelection();
    const [position, setPosition] = useState<{ top: number; left: number } | null>(null);

    useEffect(() => {
        let domRange = null;
        let rect = null;

        if (domSelection && domSelection.rangeCount > 0) {
            domRange = domSelection?.getRangeAt(0);
            rect = domRange?.getBoundingClientRect();
        }

        if (ref.current && rect && w) {
            setPosition({
                top: rect.top + w?.pageYOffset - ref.current.offsetHeight,
                left: rect.left + w?.pageXOffset - ref.current.offsetWidth / 2 + rect.width / 2,
            });
        }
    }, [selection, domSelection, w]);

    if (
        !selection ||
        !ReactEditor.isFocused(editor) ||
        Range.isCollapsed(selection) ||
        Editor.string(editor, selection) === ""
    ) {
        return null;
    }

    return (
        <>
            {createPortal(
                <Paper
                    elevation={0}
                    className={classes.paper}
                    ref={ref}
                    style={{
                        display: "inline-block",
                        position: "absolute",
                        margin: 0,
                        marginTop: -6,
                        top: position ? position.top : -1000,
                        left: position ? position.left : -1000,
                    }}
                >
                    <LeafTypeButton label={<FormatBoldIcon />} name="bold" />
                    <LeafTypeButton label={<FormatItalicIcon />} name="italic" />
                    <LeafTypeButton label={<FormatUnderlinedIcon />} name="underline" />
                    <LinkElementButton />
                </Paper>,
                document.body,
            )}
        </>
    );
};
