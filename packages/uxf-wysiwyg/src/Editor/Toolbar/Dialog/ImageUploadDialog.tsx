import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Input from "@material-ui/core/Input";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import React, { FC, useCallback, useEffect, useState } from "react";
import { Range, Transforms } from "slate";
import { ReactEditor } from "slate-react";
import { EditorImageElement, ImageUploadHandler } from "../../../types";
import { insertImage } from "../../Renderer";

export interface ImageUploadDialogProps {
    open: boolean;
    onClose: () => void;
    imageUploadHandler: ImageUploadHandler;
    imageNamespace: string;
    editor: ReactEditor;
    activeImage: EditorImageElement | null;
    selection: Range | null;
}

export const ImageUploadDialog: FC<ImageUploadDialogProps> = ({
    open,
    onClose,
    imageUploadHandler,
    imageNamespace,
    editor,
    activeImage,
    selection,
}) => {
    const [image, setImage] = useState<EditorImageElement | null>(null);
    const [caption, setCaption] = useState<string>("");
    const [source, setSource] = useState<string>("");
    const [alt, setAlt] = useState<string>("");
    const [preview, setPreview] = useState<File | null>(null);

    useEffect(() => {
        if (activeImage) {
            setSource(activeImage?.source ?? "");
            setCaption(activeImage?.caption ?? "");
            setAlt(activeImage?.alt ?? "");
        }
    }, [activeImage]);

    const uploadHandler = useCallback(
        async (e: React.ChangeEvent<HTMLInputElement>): Promise<void> => {
            e.preventDefault();
            e.persist();
            if (e?.target?.files) {
                const uploadedImage = await imageUploadHandler(e.target.files[0], imageNamespace);
                setPreview(e.target.files[0]);
                const image: EditorImageElement = {
                    type: "image",
                    name: uploadedImage.name,
                    extension: uploadedImage.extension,
                    uuid: uploadedImage.uuid,
                    namespace: uploadedImage.namespace as string,
                    url: uploadedImage.url,
                    children: [],
                };
                setImage(image);
            }
        },
        [editor],
    );

    // OK button
    const insertImageHandler = () => {
        if (image && !activeImage) {
            image.caption = caption !== "" ? caption : undefined;
            image.source = source !== "" ? source : undefined;
            image.alt = alt !== "" ? alt : undefined;

            insertImage(editor, image, selection);
            closeHandler();
        }

        if (activeImage) {
            Transforms.setNodes(
                editor,
                {
                    caption,
                    source,
                },
                { at: selection ?? undefined },
            );
            closeHandler();
        }
    };

    // Cancel button
    const closeHandler = useCallback(() => {
        resetState();
        onClose();
    }, [onClose]);

    const resetState = useCallback(() => {
        setImage(null);
        setCaption("");
        setSource("");
        setPreview(null);
        activeImage = null;
    }, []);

    return (
        <Dialog open={open} onClose={closeHandler} aria-labelledby="insert-image-dialog" fullWidth>
            <DialogTitle id="insert-image-dialog-title">
                {activeImage ? "Upravit obrázek" : "Nahrát obrázek"}
            </DialogTitle>
            <DialogContent>
                <TextField
                    autoFocus
                    margin="dense"
                    id="caption"
                    label="Popisek obrázku"
                    type="text"
                    fullWidth
                    value={caption}
                    onChange={e => setCaption(e.target.value)}
                />
                <TextField
                    margin="dense"
                    id="source"
                    label="Zdroj obrázku"
                    type="text"
                    fullWidth
                    value={source}
                    onChange={e => setSource(e.target.value)}
                />
                <TextField
                    margin="dense"
                    id="source"
                    label="Alt atribut"
                    type="text"
                    fullWidth
                    value={alt}
                    onChange={e => setAlt(e.target.value)}
                />
                {!activeImage && (
                    <Input
                        type="file"
                        onChange={uploadHandler}
                        inputProps={{ accept: "image/*" }}
                        fullWidth
                        margin="dense"
                        style={{ marginTop: 20 }}
                    />
                )}
                {preview && !activeImage && (
                    <div>
                        <img
                            src={URL.createObjectURL(preview)}
                            alt={image?.caption}
                            height={250}
                            width={555}
                            style={{ width: "100%", height: 250, objectFit: "scale-down", objectPosition: "center" }}
                        />
                        <div>
                            <Typography variant="caption">* Obrázek je zmenšen pro účely náhledu</Typography>
                        </div>
                    </div>
                )}
            </DialogContent>

            <DialogActions>
                <Button onClick={closeHandler} color="secondary">
                    Zrušit
                </Button>
                <Button onClick={insertImageHandler} color="primary" variant="contained">
                    {activeImage ? "Upravit obrázek" : "Vložit obrázek"}
                </Button>
            </DialogActions>
        </Dialog>
    );
};
