import { Checkbox } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import TextField from "@material-ui/core/TextField";
import React, { FC, useCallback, useEffect, useState } from "react";
import { Range } from "slate";
import { ReactEditor } from "slate-react";
import { EditorLinkElement } from "../../../types";
import { insertLink } from "../../Renderer";

export interface InsertLinkDialogProps {
    open: boolean;
    onClose: () => void;
    editor: ReactEditor;
    activeLink: EditorLinkElement | null;
    selection: Range | null;
}

export const InsertLinkDialog: FC<InsertLinkDialogProps> = ({ open, onClose, editor, activeLink, selection }) => {
    const [url, setUrl] = useState<string>(activeLink?.url ?? "https://");
    const [text, setText] = useState<string>("");
    const [openInNewTab, setOpenInNewTab] = useState<boolean>(false);

    let isCollapsed = selection && Range.isCollapsed(selection);

    useEffect(() => {
        if (activeLink && !isCollapsed) {
            setUrl(activeLink?.url ?? "https://");
            setOpenInNewTab(!!activeLink?.target);
        }
    }, [activeLink, open, isCollapsed]);

    useEffect(() => {
        isCollapsed = selection && Range.isCollapsed(selection);
    }, [selection]);

    // OK button
    const okButtonHandler = () => {
        editor.selection = selection;
        insertLink(editor, url, text, openInNewTab ? "_blank" : undefined);
        resetState();
        onClose();
    };

    // Cancel button
    const closeHandler = useCallback(() => {
        resetState();
        onClose();
    }, [onClose]);

    const resetState = useCallback(() => {
        setUrl("");
        setText("");
        setOpenInNewTab(false);
    }, []);

    return (
        <Dialog open={open} onClose={closeHandler} aria-labelledby="insert-link-dialog" fullWidth>
            <DialogTitle id="insert-link-dialog-title">
                {activeLink && !isCollapsed ? "Upravit odkaz" : "Vložit odkaz"}
            </DialogTitle>
            <DialogContent>
                {isCollapsed && (
                    <TextField
                        autoFocus
                        margin="dense"
                        id="text"
                        label="Text odkazu"
                        type="text"
                        fullWidth
                        value={text}
                        onChange={e => setText(e.target.value)}
                    />
                )}
                <TextField
                    margin="dense"
                    id="url"
                    label="URL odkazu"
                    type="text"
                    fullWidth
                    value={url}
                    required
                    onChange={e => setUrl(e.target.value)}
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={openInNewTab}
                            onChange={e => setOpenInNewTab(e.target.checked)}
                            name="newTab"
                        />
                    }
                    label="Otevřít v nové záložce"
                />
            </DialogContent>

            <DialogActions>
                <Button onClick={closeHandler} color="secondary">
                    Zrušit
                </Button>
                <Button onClick={okButtonHandler} color="primary" variant="contained">
                    {activeLink && !isCollapsed ? "Upravit odkaz" : "Vložit odkaz"}
                </Button>
            </DialogActions>
        </Dialog>
    );
};
