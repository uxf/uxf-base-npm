import { Theme } from "@material-ui/core/styles/createMuiTheme";
import createStyles from "@material-ui/core/styles/createStyles";
import makeStyles from "@material-ui/core/styles/makeStyles";

export const toolbarStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            display: "flex",
            alignItems: "center",
            border: `1px solid ${theme.palette.divider}`,
            flexWrap: "wrap",
            margin: theme.spacing(1, -2),
        },
        divider: {
            alignSelf: "stretch",
            height: "auto",
            margin: theme.spacing(1, 0.5),
        },
        grouped: {
            margin: theme.spacing(0.5),
            border: "none !important",
            padding: theme.spacing(0, 1),
            "&:not(:first-child)": {
                borderRadius: theme.shape.borderRadius,
            },
            "&:first-child": {
                borderRadius: theme.shape.borderRadius,
            },
            position: "sticky",
            top: 0,
        },
    }),
);
