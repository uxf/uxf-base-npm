import React, { FC } from "react";
import { DisabledActions } from "../../../types";
import { ElementTypeButton } from "../Button";
import { ButtonGroup } from "./ButtonGroup";
import FormatListBulletedIcon from "@material-ui/icons/FormatListBulleted";
import FormatListNumberedIcon from "@material-ui/icons/FormatListNumbered";

export const ListButtonGroup: FC<{ disabledActions?: DisabledActions }> = ({ disabledActions }) => {
    return (
        <ButtonGroup>
            {!disabledActions?.bulletedList && (
                <ElementTypeButton label={<FormatListBulletedIcon />} name="bulleted-list" />
            )}
            {!disabledActions?.numberedList && (
                <ElementTypeButton label={<FormatListNumberedIcon />} name="numbered-list" />
            )}
        </ButtonGroup>
    );
};
