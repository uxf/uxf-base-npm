import React, { Children, FC, ReactNode } from "react";

export const ButtonGroup: FC<{ children: ReactNode | ReactNode[] }> = ({ children }) => {
    return (
        <div>
            {Children.map(children, Child => {
                return Child;
            })}
        </div>
    );
};
