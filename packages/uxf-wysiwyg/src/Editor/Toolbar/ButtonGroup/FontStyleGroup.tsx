import CodeIcon from "@material-ui/icons/Code";
import FormatBoldIcon from "@material-ui/icons/FormatBold";
import FormatItalicIcon from "@material-ui/icons/FormatItalic";
import FormatUnderlinedIcon from "@material-ui/icons/FormatUnderlined";
import React, { FC } from "react";
import { DisabledActions } from "../../../types";
import { LeafTypeButton } from "../Button";
import { ButtonGroup } from "./ButtonGroup";

export const FontStyleGroup: FC<{ disabledActions?: DisabledActions }> = ({ disabledActions }) => {
    return (
        <ButtonGroup>
            {!disabledActions?.bold && <LeafTypeButton label={<FormatBoldIcon />} name="bold" />}
            {!disabledActions?.italic && <LeafTypeButton label={<FormatItalicIcon />} name="italic" />}
            {!disabledActions?.underline && <LeafTypeButton label={<FormatUnderlinedIcon />} name="underline" />}
            {!disabledActions?.code && <LeafTypeButton label={<CodeIcon />} name="code" />}
        </ButtonGroup>
    );
};
