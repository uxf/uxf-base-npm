import React, { FC } from "react";
import { DisabledActions } from "../../../types";
import { ElementTypeButton } from "../Button";
import { ButtonGroup } from "./ButtonGroup";

export const HeadingButtonGroup: FC<{ disabledActions?: DisabledActions }> = ({ disabledActions }) => {
    return (
        <ButtonGroup>
            {!disabledActions?.h1 && <ElementTypeButton label="H1" name="h1" />}
            {!disabledActions?.h2 && <ElementTypeButton label="H2" name="h2" />}
            {!disabledActions?.h3 && <ElementTypeButton label="H3" name="h3" />}
            {!disabledActions?.h4 && <ElementTypeButton label="H4" name="h4" />}
            {!disabledActions?.h5 && <ElementTypeButton label="H5" name="h5" />}
            {!disabledActions?.h6 && <ElementTypeButton label="H6" name="h6" />}
        </ButtonGroup>
    );
};
