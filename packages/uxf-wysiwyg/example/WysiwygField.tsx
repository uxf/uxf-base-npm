import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent/CardContent";
import CardHeader from "@material-ui/core/CardHeader/CardHeader";
import React, { FC } from "react";
import { useField } from "react-final-form";
import { ContentRenderer, WysiwygContent } from "../src/Content";
import { WysiwygEditor } from "../src/Editor";
import { ImageComponent } from "./components";

// image url mock
const imageUrlClient = (_uuid: string, _extension: string, _namespace?: string) =>
    "https://cdn.pixabay.com/photo/2020/05/20/06/47/mountain-5195052_960_720.jpg";

const imageUploadHandler = (_file: File, _namespace: string) => {
    return Promise.resolve({
        id: 1,
        uuid: "uuid",
        type: "image",
        extension: ".jpg",
        name: "name",
        namespace: "blog",
        url: "https://cdn.pixabay.com/photo/2020/05/20/06/47/mountain-5195052_960_720.jpg",
    });
};

export const WysiwygField: FC = () => {
    const { input } = useField<WysiwygContent>("wysiwyg");

    return (
        <>
            <div>
                <WysiwygEditor
                    onChange={input.onChange}
                    imageUrlClient={imageUrlClient}
                    imageNamespace="blog"
                    onUploadImage={imageUploadHandler}
                    value={input.value}
                    disabledActions={{ h1: true }}
                />
            </div>
            <br />
            <div>
                <Card>
                    <CardHeader title="Result:" />
                    <CardContent>
                        <ContentRenderer data={input.value} components={{ imageComponent: ImageComponent }} />
                    </CardContent>
                </Card>
            </div>
        </>
    );
};

WysiwygField.displayName = "WysiwygField";
