import React from "react";
import ReactDOM from "react-dom";
import { App } from "./App";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";

ReactDOM.render(
    <ThemeProvider theme={createMuiTheme()}>
        <App />
    </ThemeProvider>,
    document.getElementById("app"),
);
