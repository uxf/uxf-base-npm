import React from "react";
import { Form } from "react-final-form";
import { WysiwygField } from "./WysiwygField";

const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

const onSubmit = async (values: any) => {
    await sleep(300);
    window.alert(JSON.stringify(values));
};

export const App: React.FC = () => {
    return (
        <Form onSubmit={onSubmit}>
            {({ handleSubmit }) => {
                return (
                    <form onSubmit={handleSubmit}>
                        <div>
                            <WysiwygField />
                        </div>
                    </form>
                );
            }}
        </Form>
    );
};
