import React, { FC } from "react";
import { RenderComponentProps, RenderImageComponentProps, RenderVideoComponentProps } from "../src/Content";

export const ParagraphComponent: FC<RenderComponentProps> = ({ children }) => {
    return <p>{children}</p>;
};

export const ImageComponent: FC<RenderImageComponentProps> = ({ image }) => {
    return (
        <div style={{ marginBottom: "2em", width: "100%" }} id="custom-image-component">
            <figure contentEditable={false}>
                <img src={image.url} alt={image.caption} style={{ width: "100%" }} />
                {(image.caption || image.source) && (
                    <figcaption>
                        {image.caption}
                        {image.source && (
                            <>
                                {image.caption && <br />}
                                <cite>{image.source}</cite>
                            </>
                        )}
                    </figcaption>
                )}
            </figure>
        </div>
    );
};

export const VideoComponent: FC<RenderVideoComponentProps> = ({ children }) => {
    return <div>{children}</div>;
};
